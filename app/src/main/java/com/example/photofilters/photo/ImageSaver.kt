package com.example.photofilters.photo

import android.media.Image
import java.io.File
import java.io.FileOutputStream

internal class ImageSaver(
        private val image: Image,
        private val file: File
) : Runnable {

    override fun run() {
        val buffer = image.planes[0].buffer
        val bytes = ByteArray(buffer.remaining())
        buffer.get(bytes)
        var output: FileOutputStream? = null
        try {
            output = FileOutputStream(file).apply {
                write(bytes)
            }
        } finally {
            image.close()
            output?.close()
        }
    }
}
