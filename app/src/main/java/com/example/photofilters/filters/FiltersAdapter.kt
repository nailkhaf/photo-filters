package com.example.photofilters.filters

import android.graphics.Bitmap
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.example.photofilters.R
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.withContext

class FiltersAdapter(bitmap: Bitmap, private val callback: (filter: Filter) -> Unit) : RecyclerView.Adapter<FilterViewHolder>() {

    private val SCALE = 0.1f
    private var bmpPrev: Bitmap

    init {
        val width = (bitmap.width * SCALE).toInt()
        val height = (bitmap.height * SCALE).toInt()
        bmpPrev = Bitmap.createScaledBitmap(bitmap,
                if (width < 256) 256 else width,
                if (width < 256) (256f / width * height).toInt() else height, true)
    }

    private val filters = Filter.getFilters()

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): FilterViewHolder {
        val view = LayoutInflater.from(p0.context).inflate(R.layout.filter_item, p0, false)
        return FilterViewHolder(view)
    }

    override fun getItemCount() = filters.size

    override fun onBindViewHolder(p0: FilterViewHolder, p1: Int) = p0.bind(filters[p1], bmpPrev, callback)

}

class FilterViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    private val image = view.findViewById<ImageView>(R.id.filter_image)

    fun bind(filter: Filter, bitmapFilter: Bitmap, callback: (filter: Filter) -> Unit) {
        launch {
            val drawable = filter.filterWithCaching(bitmapFilter, itemView.context)
            withContext(UI) {
                image.setImageDrawable(drawable)
            }
        }

        image.setOnClickListener {
            callback(filter)
        }
    }
}