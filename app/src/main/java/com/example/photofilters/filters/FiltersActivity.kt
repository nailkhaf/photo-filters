package com.example.photofilters.filters

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.example.photofilters.R

class FiltersActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_container)

        val path = intent.getStringExtra(EXTRA_PATH) ?: throw IllegalStateException()

        savedInstanceState ?: supportFragmentManager.beginTransaction()
                .replace(R.id.container, FiltersFragment.newInstance(path))
                .commit()
    }

    companion object {

        private const val EXTRA_PATH = "com.example.photofilters.filters.path"

        fun start(ctx: Context, path: String) {
            val intent = Intent(ctx, FiltersActivity::class.java)
            intent.putExtra(EXTRA_PATH, path)
            ctx.startActivity(intent)
        }
    }
}
