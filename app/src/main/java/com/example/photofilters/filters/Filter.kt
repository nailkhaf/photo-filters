package com.example.photofilters.filters

import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.Drawable

interface Filter {

    suspend fun filterWithCaching(bitmap: Bitmap, ctx: Context): Drawable

    suspend operator fun invoke(bitmap: Bitmap): Bitmap

    companion object {
        fun getFilters() = listOf(
                OriginFilter(),
                InvertFilter(),
                SepiaFilter(),
                TintFilter(90),
                TintFilter(180),
                TintFilter(270),
                ColorFilter(1f, 0.4f, 0.4f),
                ColorFilter(0.4f, 1f, 0.4f),
                ColorFilter(0.4f, 0.4f, 1f),
                GrayScaleFilter(1f),
                GrayScaleFilter(0.7f)
        )
    }
}