package com.example.photofilters.filters

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.media.ExifInterface
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.*
import com.example.photofilters.R
import kotlinx.android.synthetic.main.fragment_filters.*
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.withContext


class FiltersFragment : Fragment() {

    private lateinit var originBitmap: Bitmap
    private var currentFilter: Filter? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.fragment_filters, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(activity as AppCompatActivity) {
            setSupportActionBar(toolbar)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setHomeAsUpIndicator(R.drawable.back)
        }

        val path = arguments!!.getString(PATH_ARG)
        originBitmap = applyHD(BitmapFactory.decodeFile(path))

        val ei = ExifInterface(path)
        val orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_UNDEFINED)

        originBitmap = when (orientation) {
            ExifInterface.ORIENTATION_ROTATE_90 -> rotateImage(originBitmap, 90f)
            ExifInterface.ORIENTATION_ROTATE_180 -> rotateImage(originBitmap, 180f)
            ExifInterface.ORIENTATION_ROTATE_270 -> rotateImage(originBitmap, 270f)
            ExifInterface.ORIENTATION_NORMAL -> originBitmap
            else -> originBitmap
        }

        image.setImageBitmap(originBitmap)

        filters.adapter = FiltersAdapter(originBitmap) {
            showProgress()
            launch {
                val bitmap = it(originBitmap)
                currentFilter = it
                withContext(UI) {
                    image.setImageBitmap(bitmap)
                    hideProgress()
                }
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean = when (item!!.itemId) {
        R.id.share -> {
            showProgress()
            launch {
                val filter = currentFilter
                val bitmap = if (filter != null) {
                    filter(originBitmap)
                } else {
                    originBitmap
                }

                val permission = ContextCompat.checkSelfPermission(activity!!, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                if (permission != PackageManager.PERMISSION_GRANTED) {
                    launch(UI) { showProgress() }
                    return@launch
                }
                val image = MediaStore.Images.Media.insertImage(activity!!.contentResolver, bitmap, "img", null)
                val uri = Uri.parse(image)

                val shareIntent: Intent = Intent().apply {
                    action = Intent.ACTION_SEND
                    putExtra(Intent.EXTRA_STREAM, uri)
                    type = "image/jpeg"
                }
                withContext(UI) {
                    hideProgress()
                    startActivity(Intent.createChooser(shareIntent, resources.getText(R.string.send_to)))
                }
            }
            true
        }
        android.R.id.home -> {
            activity!!.finish()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    private fun showProgress() {
        progress.visibility = View.VISIBLE
    }

    private fun hideProgress() {
        progress.visibility = View.GONE
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater!!.inflate(R.menu.filters, menu)
    }

    companion object {
        private const val PATH_ARG = "path"

        fun newInstance(path: String) = FiltersFragment().apply {
            arguments = Bundle().apply {
                putString(PATH_ARG, path)
            }
        }

        fun applyHD(source: Bitmap): Bitmap {
            val width = source.width
            val height = source.height
            val isLandScape = width > height

            val appliedWidth = if (!isLandScape) {
                if (width > 1280) 1280 else width
            } else {
                if (height > 1280) (1280f / height * width).toInt() else width
            }
            val appliedHeight = if (!isLandScape) {
                if (width > 1280) (1280f / height * width).toInt() else height
            } else {
                if (height > 1280) 1280 else height
            }

            return Bitmap.createScaledBitmap(source,
                    appliedWidth,
                    appliedHeight,
                    true)
        }

        fun rotateImage(source: Bitmap, angle: Float): Bitmap {
            val matrix = Matrix()
            matrix.postRotate(angle)
            val width = source.width
            val height = source.height
            return Bitmap.createBitmap(source, 0, 0, width, height, matrix, true)
        }
    }
}


