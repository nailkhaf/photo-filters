@file:Suppress("LocalVariableName")

package com.example.photofilters.filters

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import kotlinx.coroutines.experimental.Deferred
import kotlinx.coroutines.experimental.async
import kotlinx.coroutines.experimental.awaitAll

private const val GS_RED = 0.299f
private const val GS_GREEN = 0.587f
private const val GS_BLUE = 0.114f

typealias OriginFilter = DumbFilter

class GrayScaleFilter(
        private val depth: Float = 1f
) : Filter {
    private var cached = false

    private lateinit var img: Drawable
    override suspend fun filterWithCaching(bitmap: Bitmap, ctx: Context): Drawable {
        if (cached) return img
        val bmp = invoke(bitmap)
        cached = true
        img = BitmapDrawable(ctx.resources, bmp)
        return img
    }

    override suspend fun invoke(bitmap: Bitmap): Bitmap = Bitmap.createBitmap(bitmap.copy(bitmap.config, true)).apply {
        val tasks = mutableListOf<Deferred<Any>>()
        for (x in 0 until width) {
            tasks += async {
                for (y in 0 until height) {
                    val pixel = getPixel(x, y)
                    val R = Color.red(pixel)
                    val G = Color.green(pixel)
                    val B = Color.blue(pixel)

                    var GS = ((GS_RED * R + GS_GREEN * G + GS_BLUE * B).toInt() * depth).toInt()
                    GS = if (GS < 0) 0 else GS
                    GS = if (GS > 255) 255 else GS
                    setPixel(x, y, Color.rgb(GS, GS, GS))
                }
            }
        }
        awaitAll(*tasks.toTypedArray())
    }
}

class DumbFilter : Filter {
    private var cached = false

    private lateinit var img: Drawable
    override suspend fun filterWithCaching(bitmap: Bitmap, ctx: Context): Drawable {
        if (cached) return img

        img = BitmapDrawable(ctx.resources, bitmap)
        cached = true
        return img
    }

    override suspend fun invoke(bitmap: Bitmap): Bitmap = bitmap
}

class InvertFilter : Filter {
    private var cached = false

    private lateinit var img: Drawable
    override suspend fun filterWithCaching(bitmap: Bitmap, ctx: Context): Drawable {
        if (cached) return img

        val bmp = Bitmap.createBitmap(bitmap.copy(bitmap.config, true)).apply {
            val tasks = mutableListOf<Deferred<Any>>()
            for (x in 0 until width) {
                tasks += async {
                    for (y in 0 until height) {
                        val pixel = getPixel(x, y)
                        val R = 255 - Color.red(pixel)
                        val G = 255 - Color.green(pixel)
                        val B = 255 - Color.blue(pixel)

                        setPixel(x, y, Color.rgb(R, G, B))
                    }
                }
            }
            awaitAll(*tasks.toTypedArray())
        }

        img = BitmapDrawable(ctx.resources, bmp)
        cached = true
        return img
    }

    override suspend fun invoke(bitmap: Bitmap): Bitmap = Bitmap.createBitmap(bitmap.copy(bitmap.config, true)).apply {
        val tasks = mutableListOf<Deferred<Any>>()

        for (x in 0 until width) {
            tasks += async {
                for (y in 0 until height) {
                    val pixel = getPixel(x, y)
                    val R = 255 - Color.red(pixel)
                    val G = 255 - Color.green(pixel)
                    val B = 255 - Color.blue(pixel)

                    setPixel(x, y, Color.rgb(R, G, B))
                }
            }
        }
        awaitAll(*tasks.toTypedArray())
    }
}

class SepiaFilter : Filter {
    private var cached = false

    private lateinit var img: Drawable
    override suspend fun filterWithCaching(bitmap: Bitmap, ctx: Context): Drawable {
        if (cached) return img

        val bmp = invoke(bitmap)

        img = BitmapDrawable(ctx.resources, bmp)
        cached = true
        return img
    }

    override suspend fun invoke(bitmap: Bitmap): Bitmap = Bitmap.createBitmap(bitmap.copy(bitmap.config, true)).apply {
        val tasks = mutableListOf<Deferred<Any>>()

        for (x in 0 until width) {
            tasks += async {
                for (y in 0 until height) {
                    val pixel = getPixel(x, y)
                    val R = Color.red(pixel)
                    val G = Color.green(pixel)
                    val B = Color.blue(pixel)

                    var tr = (0.393 * R + 0.769 * G + 0.189 * B).toInt()
                    var tg = (0.349 * R + 0.686 * G + 0.168 * B).toInt()
                    var tb = (0.272 * R + 0.534 * G + 0.131 * B).toInt()

                    tr = if (tr > 255) 255 else tr
                    tg = if (tg > 255) 255 else tg
                    tb = if (tb > 255) 255 else tb

                    setPixel(x, y, Color.rgb(tr, tg, tb))
                }
            }
        }
        awaitAll(*tasks.toTypedArray())
    }
}

class TintFilter(private val degree: Int = 50) : Filter {
    private var cached = false

    private lateinit var img: Drawable
    override suspend fun filterWithCaching(bitmap: Bitmap, ctx: Context): Drawable {
        if (cached) return img

        val bmp = invoke(bitmap)

        img = BitmapDrawable(ctx.resources, bmp)
        cached = true
        return img
    }

    override suspend fun invoke(bitmap: Bitmap): Bitmap = Bitmap.createBitmap(bitmap.copy(bitmap.config, true)).apply {
        val angle = (3.14 * degree) / 180
        val S = (256 * Math.sin(angle)).toInt()
        val C = (256 * Math.cos(angle)).toInt()

        val tasks = mutableListOf<Deferred<Any>>()

        for (x in 0 until width) {
            tasks += async {
                for (y in 0 until height) {
                    val pixel = getPixel(x, y)

                    var R = Color.red(pixel)
                    var G = Color.green(pixel)
                    var B = Color.blue(pixel)

                    val RY = (70 * R - 59 * G - 11 * B) / 100
                    val GY = (-30 * R + 41 * G - 11 * B) / 100
                    val BY = (-30 * R - 59 * G + 89 * B) / 100

                    val Y = (30 * R + 59 * G + 11 * B) / 100
                    val RYY = (S * BY + C * RY) / 256
                    val BYY = (C * BY - S * RY) / 256
                    val GYY = (-51 * RYY - 19 * BYY) / 100

                    R = Y + RYY
                    R = if (R < 0) 0 else if (R > 255) 255 else R
                    G = Y + GYY
                    G = if (G < 0) 0 else if (G > 255) 255 else G
                    B = Y + BYY
                    B = if (B < 0) 0 else if (B > 255) 255 else B

                    setPixel(x, y, Color.rgb(R, G, B))
                }
            }
        }
        awaitAll(*tasks.toTypedArray())
    }
}

class ColorFilter(
        private val red: Float,
        private val green: Float,
        private val blue: Float
) : Filter {
    private var cached = false

    private lateinit var img: Drawable
    override suspend fun filterWithCaching(bitmap: Bitmap, ctx: Context): Drawable {
        if (cached) return img

        val bmp = invoke(bitmap)

        img = BitmapDrawable(ctx.resources, bmp)
        cached = true
        return img
    }

    override suspend fun invoke(bitmap: Bitmap): Bitmap = Bitmap.createBitmap(bitmap.copy(bitmap.config, true)).apply {
        val tasks = mutableListOf<Deferred<Any>>()

        for (x in 0 until width) {
            tasks += async {
                for (y in 0 until height) {
                    val pixel = getPixel(x, y)

                    val R = (Color.red(pixel) * red).toInt()
                    val G = (Color.green(pixel) * green).toInt()
                    val B = (Color.blue(pixel) * blue).toInt()

                    setPixel(x, y, Color.rgb(R, G, B))
                }
            }
        }
        awaitAll(*tasks.toTypedArray())
    }
}
